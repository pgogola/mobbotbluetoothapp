package com.example.piotr.bluetooth;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.piotr.view.ControlPanelViewListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlPanelCoordinates extends Fragment implements ControlPanelViewListener {

    private IControlPanelListener controlPanelListener_ = null;
    private Button stopButton_ = null;
    private Button executeButton_ = null;

    private EditText x_ = null;
    private EditText y_ = null;
    private EditText theta_ = null;

    private final float L_ = 15.0f;
    private final float R_ = 3.0f;
    private final double OBW_ = R_*Math.PI;

    private final double maxPhi_ = 10*Math.PI;

    private boolean isExecutingInstruction_ = false;

    private double degreesToRad(double d){
        return Math.PI*d/360;
    }

    public ControlPanelCoordinates() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        stopButton_ = getActivity().findViewById(R.id.id_button_coordinate_stop);
        executeButton_ = getActivity().findViewById(R.id.id_button_coordinate_execute);

        x_ = getActivity().findViewById(R.id.id_x_coordinate);
        y_ = getActivity().findViewById(R.id.id_y_coordinate);
        theta_ = getActivity().findViewById(R.id.id_theta_coordinate);

        executeButton_.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int a = event.getAction();
                if (MotionEvent.ACTION_UP == event.getAction()) {
                    Log.i("ControlPanelCoord", "execute button on touch");
                    final double wantedTheta = degreesToRad(Double.parseDouble(theta_.getText().toString()));
                    Runnable thread = new Runnable() {
                        @Override
                        public void run() {
                            double currentTheta = 0;
                            double error = Double.MAX_VALUE;
                            while(isExecutingInstruction_ && Math.abs(error)>0.01){
                                error = wantedTheta-currentTheta;
                                Log.i("ControlPanelCoord", "ERROR " + error);
                                double phi = Math.PI*L_/18;
                                if(error<0)
                                {
                                    phi*=-1;
                                }
                                double theta_d = (1/L_)*(R_)*phi;
                                onControlPanelAction(0, (int)(phi*400*Math.PI/100.f));
                                currentTheta += theta_d*0.05;
                                try {
                                    Thread.sleep(50);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            onControlPanelAction(0, 0);
                            isExecutingInstruction_ = false;
                        }
                    };
                    isExecutingInstruction_ = true;
                    new Thread(thread).start();
                }
                return false;
            }
        });

        stopButton_.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_BUTTON_RELEASE == event.getAction()) {
                    Log.i("ControlPanelCoord", "stop button on touch");
                    isExecutingInstruction_ = false;
                }
                return false;
            }
        });


    }

    public void addControlPanelListener(IControlPanelListener controlPanelListener) {
        controlPanelListener_ = controlPanelListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_control_panel_coordinates, container, false);
    }

    @Override
    public void onControlPanelAction(float forward, float turn) {
        controlPanelListener_.onControlPanelValueChanged(forward, turn);
    }
}
