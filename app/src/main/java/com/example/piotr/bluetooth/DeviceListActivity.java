package com.example.piotr.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Set;

public class DeviceListActivity extends AppCompatActivity {

    private BluetoothAdapter bluetoothAdapter = null;
    private ArrayAdapter<String> arrayAdapter = null;
    private ArrayList<String> devicesAddresses = null;

    private Thread discoveringInfo = null;
    private ProgressBar discoveringProgressBar = null;

    private Intent result = null;

    private View selectedDeviceView_ = null;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                arrayAdapter.add(deviceName);
                devicesAddresses.add(device.getAddress());
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_list);
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        ListView devicesListView = findViewById(R.id.id_devices_list);
        Set<BluetoothDevice> boundedDevices = bluetoothAdapter.getBondedDevices();
        ArrayList<String> devicesName = new ArrayList<>();
        devicesAddresses = new ArrayList<>();
        for (BluetoothDevice boundedDevice : boundedDevices) {
            devicesName.add(boundedDevice.getName());
            devicesAddresses.add(boundedDevice.getAddress());
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, devicesName);
        devicesListView.setAdapter(arrayAdapter);

        discoveringProgressBar = findViewById(R.id.id_discovering_progress_bar);

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(mReceiver, filter);

        result = new Intent();
        setResult(Activity.RESULT_OK, result);

        devicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(null!=selectedDeviceView_) {
                    selectedDeviceView_.setBackgroundResource(R.color.white);
                }
                view.setBackgroundResource(R.color.colorSelectedDevice);
                selectedDeviceView_ = view;
                result.putExtra(MainActivity.DEVICE_ADDRESS_TAG, devicesAddresses.get(position));
                result.putExtra(MainActivity.DEVICE_NAME_TAG, arrayAdapter.getItem(position));
                Toast.makeText(getApplicationContext(),
                        getResources().getString(R.string.device_selected)
                                + arrayAdapter.getItem(position), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClick_button_back(View view) {
        finish();
    }

    public void onClick_refresh(View view) {
        arrayAdapter.clear();
        devicesAddresses.clear();
        bluetoothAdapter.startDiscovery();
        discoveringProgressBar.setVisibility(View.VISIBLE);
        Toast.makeText(getApplicationContext(), R.string.searching_device,
                Toast.LENGTH_SHORT).show();
        selectedDeviceView_ = null;
    }

    @Override
    public void finish() {
        bluetoothAdapter.cancelDiscovery();
        if (null != discoveringInfo) {
            try {
                discoveringInfo.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            discoveringInfo = null;
        }
        unregisterReceiver(mReceiver);
        super.finish();
    }

    class DiscoveringInfo implements Runnable {
        private View view;

        DiscoveringInfo(View view) {
            this.view = view;
        }

        @Override
        public void run() {
            while (bluetoothAdapter.isDiscovering()) {
            }
            view.setVisibility(View.INVISIBLE);
            discoveringInfo = null;
        }
    }


}
