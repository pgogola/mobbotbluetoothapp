package com.example.piotr.bluetooth;

public interface IControlPanelListener {
    void onControlPanelValueChanged(float forward, float turn);
}
