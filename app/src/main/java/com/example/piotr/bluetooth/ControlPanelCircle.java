package com.example.piotr.bluetooth;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.piotr.view.ControlPanelView;
import com.example.piotr.view.ControlPanelViewListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlPanelCircle extends Fragment implements ControlPanelViewListener {

    private IControlPanelListener controlPanelListener_;
    private ControlPanelView controlPanelView_ = null;
    public ControlPanelCircle() {
        // Required empty public constructor
    }

    public void addControlPanelListener(IControlPanelListener controlPanelListener) {
        controlPanelListener_ = controlPanelListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_control_panel_circle, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        controlPanelView_ = getView().findViewById(R.id.id_control_panel_view);
        controlPanelView_.addControlPanelViewListener(this);
    }

    @Override
    public void onControlPanelAction(float forward, float turn) {
        controlPanelListener_.onControlPanelValueChanged(forward, turn);
    }
}
