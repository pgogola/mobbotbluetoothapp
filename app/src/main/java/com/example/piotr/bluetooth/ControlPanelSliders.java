package com.example.piotr.bluetooth;


import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;


/**
 * A simple {@link Fragment} subclass.
 */
public class ControlPanelSliders extends Fragment{

    private IControlPanelListener controlPanelListener_;
    private SeekBar frontBackSlider_ = null;
    private SeekBar rightLeftSlider_ = null;

    private int progressBarFrontBackProgress_ = 0;
    private int progressBarRightLeftProgress_ = 0;


    public ControlPanelSliders() {
        // Required empty public constructor
    }

    public void addControlPanelListener(IControlPanelListener controlPanelListener) {
        controlPanelListener_ = controlPanelListener;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_control_panel_sliders, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        frontBackSlider_ = getActivity().findViewById(R.id.id_seek_bar_front_back);
        rightLeftSlider_ = getActivity().findViewById(R.id.id_seek_bar_right_left);
        frontBackSlider_.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressBarFrontBackProgress_ = progress-100;
                controlPanelListener_.onControlPanelValueChanged(progressBarFrontBackProgress_,
                        progressBarRightLeftProgress_);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(100);
            }
        });
        rightLeftSlider_.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressBarRightLeftProgress_ = progress-100;
                controlPanelListener_.onControlPanelValueChanged(progressBarFrontBackProgress_,
                        progressBarRightLeftProgress_);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.setProgress(100);
            }
        });
    }
}
