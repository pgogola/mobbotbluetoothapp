package com.example.piotr.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements IControlPanelListener {
    static final public String DEVICE_ADDRESS_TAG = "DEVICE_ADDRESS";
    static final public String DEVICE_NAME_TAG = "DEVICE_NAME";

    static private final UUID BT_MODULE_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    static private final String[] PERMISSIONS = {"android.permission.ACCESS_COARSE_LOCATION"};
    static private final int DEVICE_ADDRESS = 10;

    static private final String TAG_FRAGMENT_CIRCLE = "CONTROL_PANEL_CIRCLE";
    static private final String TAG_FRAGMENT_SLIDER = "CONTROL_PANEL_SLIDER";
    static private final String TAG_FRAGMENT_COORDINATES = "CONTROL_PANEL_COORDINATES";
    static private final String TAG_FRAGMENT_BLANK = "CONTROL_PANEL_BLANK";

    private String selectedFragment_ = TAG_FRAGMENT_SLIDER;
    private String currentFragment_ = TAG_FRAGMENT_BLANK;

    private Button findDevicesButton_ = null;
    private Button connectButton_ = null;
    private TextView deviceNameTextView_ = null;
    private TextView deviceNameLabelTextView_ = null;

    private BluetoothAdapter bluetoothAdapter_ = null;
    private BluetoothSocket bluetoothSocket_ = null;
    private BluetoothDevice bluetoothDevice_ = null;
    private String deviceAddress_ = null;

    private OutputStream outputStream_ = null;

    private ContinuousSender continuousSender_ = null;

    private int rightWheel_ = 0;
    private int leftWheel_ = 0;

    private boolean isConnected_ = false;

    private ControlPanelCircle controlPanelCircle_ = null;
    private ControlPanelSliders controlPanelSliders_ = null;
    private ControlPanelCoordinates controlPanelCoordinates_ = null;
    private ControlPanelBlank controlPanelBlank_ = null;

    private FragmentTransaction fragmentTransaction_ = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bluetoothAdapter_ = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter_.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 1);
        }
        ActivityCompat.requestPermissions(this, PERMISSIONS, 100);

        findDevicesButton_ = findViewById(R.id.id_find_device_button);
        connectButton_ = findViewById(R.id.id_connect_button);

        deviceNameTextView_ = findViewById(R.id.id_paired_device);
        deviceNameLabelTextView_ = findViewById(R.id.id_paired_device_label);

        controlPanelCircle_ = new ControlPanelCircle();
        controlPanelSliders_ = new ControlPanelSliders();
        controlPanelCoordinates_ = new ControlPanelCoordinates();
        controlPanelBlank_ = new ControlPanelBlank();

        controlPanelCircle_.addControlPanelListener(this);
        controlPanelSliders_.addControlPanelListener(this);
        controlPanelCoordinates_.addControlPanelListener(this);

        fragmentTransaction_ = getSupportFragmentManager().beginTransaction();
        fragmentTransaction_.add(R.id.id_control_fragment, controlPanelCircle_, TAG_FRAGMENT_CIRCLE);
        fragmentTransaction_.detach(controlPanelCircle_);
        fragmentTransaction_.add(R.id.id_control_fragment, controlPanelSliders_, TAG_FRAGMENT_SLIDER);
        fragmentTransaction_.detach(controlPanelSliders_);
        fragmentTransaction_.add(R.id.id_control_fragment, controlPanelCoordinates_, TAG_FRAGMENT_COORDINATES);
        fragmentTransaction_.detach(controlPanelCoordinates_);
        fragmentTransaction_.add(R.id.id_control_fragment, controlPanelBlank_, TAG_FRAGMENT_BLANK);
        fragmentTransaction_.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final String prevFragment = currentFragment_;
        switch (item.getItemId()) {
            case R.id.id_menu_control_panel_circle:
                selectedFragment_ = TAG_FRAGMENT_CIRCLE;
                break;
            case R.id.id_menu_control_panel_slider:
                selectedFragment_ = TAG_FRAGMENT_SLIDER;
                break;
            case R.id.id_menu_control_panel_coordinates:
                selectedFragment_ = TAG_FRAGMENT_COORDINATES;
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        if (isConnected_) {
            currentFragment_ = selectedFragment_;
            fragmentTransaction_ = getSupportFragmentManager().beginTransaction();
            fragmentTransaction_.detach(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag(prevFragment)));
            fragmentTransaction_.attach(Objects.requireNonNull(getSupportFragmentManager().findFragmentByTag(selectedFragment_)));
            fragmentTransaction_.commit();
        }
        return true;
    }

    public void onClick_findDeviceAndPair(View view) {
        Intent activityList = new Intent(this, DeviceListActivity.class);
        startActivityForResult(activityList, DEVICE_ADDRESS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case DEVICE_ADDRESS:
                assert data != null;
                deviceAddress_ = data.getStringExtra(DEVICE_ADDRESS_TAG);
                String deviceName = data.getStringExtra(DEVICE_NAME_TAG);
                if (null != deviceName) {
                    deviceNameTextView_.setText(deviceName);
                    connectButton_.setEnabled(true);
                } else {
                    deviceNameTextView_.setText(R.string.no_device);
                    connectButton_.setEnabled(false);
                }
                break;
        }
    }

    public void onClick_connectButton(View view) {
        if (null == bluetoothSocket_ || !bluetoothSocket_.isConnected()) {
            Toast toast = Toast.makeText(getApplicationContext(),
                    R.string.connecting_in_progress, Toast.LENGTH_LONG);
            toast.show();
            bluetoothDevice_ = bluetoothAdapter_.getRemoteDevice(deviceAddress_);
            try {
                bluetoothSocket_ = bluetoothDevice_.createRfcommSocketToServiceRecord(BT_MODULE_UUID);
                bluetoothSocket_.connect();
                outputStream_ = bluetoothSocket_.getOutputStream();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),
                        "Nawiązanie połączenia zakończone niepowodzeniem.",
                        Toast.LENGTH_LONG).show();
                bluetoothDevice_ = null;
                bluetoothSocket_ = null;
                outputStream_ = null;
                return;
            }
        } else {
            try {
                bluetoothSocket_.close();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(),
                        "Zakończenie połączenia nieudane.",
                        Toast.LENGTH_LONG).show();
                return;
            }
            bluetoothSocket_ = null;
            bluetoothDevice_ = null;
        }
        isConnected_ = null != bluetoothSocket_ && bluetoothSocket_.isConnected();
        if (isConnected_) {
            deviceNameLabelTextView_.setText(R.string.paired_device_label_text_view);
            connectButton_.setText(R.string.disconnect_button);

            fragmentTransaction_ = getSupportFragmentManager().beginTransaction();
            fragmentTransaction_.detach(Objects.requireNonNull(getSupportFragmentManager().
                    findFragmentByTag(currentFragment_)));
            fragmentTransaction_.attach(Objects.requireNonNull(getSupportFragmentManager().
                    findFragmentByTag(selectedFragment_)));
            fragmentTransaction_.commit();

            continuousSender_ = new ContinuousSender();
            continuousSender_.start();
        } else {
            deviceNameLabelTextView_.setText(R.string.selected_device_label_text_view);
            connectButton_.setText(R.string.connect_button);
            outputStream_ = null;
            continuousSender_ = null;
            fragmentTransaction_ = getSupportFragmentManager().beginTransaction();
            fragmentTransaction_.detach(Objects.requireNonNull(getSupportFragmentManager().
                    findFragmentByTag(selectedFragment_)));
            fragmentTransaction_.attach(Objects.requireNonNull(getSupportFragmentManager().
                    findFragmentByTag(TAG_FRAGMENT_BLANK)));
            fragmentTransaction_.commit();

        }
        findDevicesButton_.setEnabled(!isConnected_);
    }

    @Override
    public void onControlPanelValueChanged(float forward, float turn) {
        int forwardValue = (int) forward;
        int turnValue = (int) (forwardValue >= 0 ? turn : -turn);
        Log.i("onControlPanelAction", "received data: forward=" + forwardValue + ", turn=" + turnValue);

        if (0 != forwardValue) {
            rightWheel_ = forwardValue / 2 - Math.min(turnValue / 2, 0);
            leftWheel_ = forwardValue / 2 + Math.max(turnValue / 2, 0);
        } else {
            rightWheel_ = -turnValue / 2;
            leftWheel_ = turnValue / 2;
        }
        Log.i("onControlPanelAction", "right=" + rightWheel_ + ", left=" + leftWheel_);
    }

    public class ContinuousSender extends Thread {
        @Override
        public void run() {
            super.run();
            while (isConnected_) {
                try {
                    Log.i("RL", "" + (byte) rightWheel_ + " " + (byte) leftWheel_);
                    outputStream_.write(new byte[]{101, (byte) rightWheel_, (byte) leftWheel_});
                    sleep(250);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

