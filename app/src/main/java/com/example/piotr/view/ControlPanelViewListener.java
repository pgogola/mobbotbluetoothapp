package com.example.piotr.view;

public interface ControlPanelViewListener {
    void onControlPanelAction(float forward, float turn);
}
