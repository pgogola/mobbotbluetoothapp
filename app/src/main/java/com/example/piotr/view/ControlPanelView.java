package com.example.piotr.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.example.piotr.bluetooth.R;

public class ControlPanelView extends View {

    static private boolean isInsideCircle(float a, float b, float r, float x, float y) {
        return Math.pow(x - a, 2) + Math.pow(y - b, 2) <= r * r;
    }

    private final float STROKE_WIDTH = 10;

    private float x_ = 0;
    private float y_ = 0;
    private float offsetX_ = 0;
    private float offsetY_ = 0;
    private float r_ = 0;
    private Paint paint_ = new Paint();
    private ControlPanelViewListener listener_ = null;

    public void addControlPanelViewListener(ControlPanelViewListener listener) {
        listener_ = listener;
    }

    public ControlPanelView(Context context) {
        super(context);
    }

    public ControlPanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ControlPanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ControlPanelView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        paint_.setColor(ResourcesCompat.getColor(getResources(), R.color.colorOutsideCircle, null));
        paint_.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawCircle(getWidth() / 2, getHeight() / 2,
                Math.min(getWidth(), getHeight()) / 2, paint_);
        paint_.setColor(ResourcesCompat.getColor(getResources(), R.color.colorInsideCircle, null));
        paint_.setStyle(Paint.Style.STROKE);
        paint_.setStrokeWidth(STROKE_WIDTH);
        canvas.drawCircle(x_, y_,
                Math.min(getWidth() / 3, getHeight()) / 3, paint_);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        x_ = offsetX_ = getWidth() / 2;
        y_ = offsetY_ = getHeight() / 2;
        r_ = Math.min(offsetX_, offsetY_);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        x_ = event.getX();
        y_ = event.getY();
        Log.i("ControlPanelView", "onTouchEvent x=" + x_ + ", y=" + y_);
        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                x_ = offsetX_;
                y_ = offsetY_;
                break;
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                if (!isInsideCircle(offsetX_, offsetY_, r_, x_, y_)) {
                    return true;
                }
        }
        if (null != listener_) {
            listener_.onControlPanelAction(-100 * (y_ - offsetY_) / offsetY_,
                    100 * (x_ - offsetX_) / offsetX_);
            Log.i("CPV: ", ""+y_ + " " + x_);
        }
        invalidate();
        return true;
    }
}
